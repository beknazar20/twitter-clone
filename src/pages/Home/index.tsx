import React from 'react';
import { Container, Grid, InputAdornment, Paper, Typography } from '@material-ui/core';
import SearchIcon from '@material-ui/icons/SearchOutlined';
import PersonAddIcon from '@material-ui/icons/PersonAddOutlined';
import CircularProgress from '@material-ui/core/CircularProgress';
import ListItem from '@material-ui/core/ListItem/ListItem';
import Divider from '@material-ui/core/Divider/Divider';
import ListItemAvatar from '@material-ui/core/ListItemAvatar/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar/Avatar';
import ListItemText from '@material-ui/core/ListItemText/ListItemText';
import List from '@material-ui/core/List/List';
import Button from '@material-ui/core/Button/Button';
import {Tags} from '../../components/Tags'
import { AddTweetForm } from '../../components/AddTweetForm';
import { Tweet } from '../../components/Tweet';
import { SideMenu } from '../../components/SideMenu';
import { useHomeStyles } from './theme';
import { SearchTextField } from '../../components/SearchTextField';
import { useDispatch, useSelector } from 'react-redux';
import { fetchTweets } from '../../store/ducks/tweets/actionCreators';
import { selectIsTweetsLoading, selectTweetsItems } from '../../store/ducks/tweets/selectors';
import { fetchTags } from '../../store/ducks/tags/actionCreators';
import { Route } from 'react-router-dom'
import {BackButton} from '../../components/BackButton'
import { FullTweet } from './component/FullTweet';
export const Home = (): React.ReactElement => {
  const classes = useHomeStyles();
  const dispatch = useDispatch();
  const tweets = useSelector(selectTweetsItems);
  const isLoading = useSelector(selectIsTweetsLoading);

  React.useEffect(() => {
    dispatch(fetchTweets());
    dispatch(fetchTags());
  }, [dispatch]);

  return (
    <Container className={classes.wrapper} maxWidth="lg">
      <Grid container spacing={3}>
        <Grid sm={1} md={3} item>
          <SideMenu classes={classes} />
        </Grid>
        <Grid sm={8} md={6} item>
          <Paper className={classes.tweetsWrapper} variant="outlined">
            <Paper className={classes.tweetsHeader} variant="outlined">
              <Route path="/home/:any">
               <BackButton/>
              </Route>
            <Route path={['/home', '/home/search']} exact>
                <Typography variant="h6">Твиты</Typography>
              </Route>     

            <Route path='/home/tweet'>
                <Typography variant="h6">Твитнуть</Typography>
              </Route>              
            </Paper>
           
            <Route path={['/home', '/home/search']} exact>
              <Paper>
                <div className={classes.addForm}>
                  <AddTweetForm classes={classes} />
                </div>
                <div className={classes.addFormBottomLine} />
              </Paper>
            </Route>
            <Route path="/home" exact>
           {isLoading ? (
              <div className={classes.tweetsCentred}>
                <CircularProgress />
              </div>
            ) : (
              tweets.map((tweet) => (
                <Tweet  key={tweet._id} {...tweet} classes={classes} />
              ))
            )}
         </Route>

         <Route path="/home/tweet/:id" component={FullTweet} exact />
         </Paper>
        </Grid>
        <Grid sm={3} md={3} item>
          <div className={classes.rightSide}>
            <SearchTextField
              variant="outlined"
              placeholder="Поиск по Твиттеру"
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <SearchIcon />
                  </InputAdornment>
                ),
              }}
              fullWidth
            />
        <Tags  classes={classes}/>
            <Paper className={classes.rightSideBlock}>
              <Paper className={classes.rightSideBlockHeader} variant="outlined">
                <b>Кого читать</b>
              </Paper>
              <List>
                <ListItem className={classes.rightSideBlockItem}>
                  <ListItemAvatar>
                    <Avatar
                      alt="Remy Sharp"
                      src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBIMDAkJDBIJCQkJChkICAoKCh8JCggMGBQZGSUUJBYcLi4lHCwrHyQkJjgmKy8xNTU1GiQ7QDszPy40NTEBDAwMDw8PHg8PEDcrGSs/MTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExPzExMTExMTExMTE/MTE/Mf/AABEIAMgAyAMBIgACEQEDEQH/xAAcAAABBQEBAQAAAAAAAAAAAAAEAAIDBQYBBwj/xAAwEAACAgEDAwMEAgICAgMAAAABAgARAwQSIQUxQQZRYRMiMnGBkRShUsFi4QcVI//EABoBAAIDAQEAAAAAAAAAAAAAAAECAAMEBQb/xAAnEQACAgEEAgICAwEBAAAAAAAAAQIRAwQSITETQSJRBTJCYXFTI//aAAwDAQACEQMRAD8AowMTdmBnG0an8efaYrDrWx8gsYbi9RZEoVcgbNIdIV7CcUMh3V25gWk9Q7qDUPeWmLX4nrcygmQNhGn66+GuO0tNL6uewCOPMqWx4n7MIw6QD8efaAlWbjReolei5Cn9y2x9UxOL3Lx8zyzJgdefuAkOTqLYFIDN/cZgaPQOvepcemR9rKXA+0X3M8i656nyax3DClBpfkQfqebLq3+ze/vXMJ6b6dfPX1FK/wAd4HJBUWwDSOhtmIB7ws51r7TuqXbejOBW6S6f0mU8ExPLFDeGRmVylibj0Kn8jU2ul9JhvyFQp/QqOLBNyLNFh8TMhoMzYmD4Rvrk14m56B6iLVjyfaRxA8XpptJuCqWDcHi6gebpxwOuTlTdnxH3CODPR1cMAw5vmLZ5lR0TW7lVD4FS33f+owjInXdGMtSaq5jXFyBIKnGEkqNYSAIGEYyyYiNIkIDETskKxSBPBSZwRoE7dQUAcSR2udXIwN239xoM4RIEssHU2SuTxLHTepGWhVzOVcXY8QEXRtP/AL36iN2uuIFg076t6o7SaP6ld0jSvlyIKO0meqdE6YmFFNAkizK5zosxx3A/p70qiKHaiTzzNKvTUQClUV8STA4XgQhnsSq7L6roHXTr2oRNpl9hHdjHkExaQ1sjx4gPAEMwgL25g5WSYmqGKQGwlwGBBAmf6r01Xs9q5EvWeDagbgZZddC02ZDTv/j5AvYXU1Wnyh0Qg2auZXrePadw4rmEemNb9Vjju9vEtiyiUTS5BxIzcIbtIAeY5WNqNaSESNpCDCIxo8xpEhCOop2KEh8/AXyZ0AnhRcafu4HfxN56M9MjVY/quKrkQJJsjfBhmxuv5AiL9z07r3phMaM4oECeb67FscgeDUs8dIVSt0iAiJOXUTgPiSYV/wD1xj3MqfDGVvs9B9K6QFQ1D3m60y0oAmW9LqFxAnjiWuo6wMPAIMzTts2RpRNBhxeTCCQKH8TDaj1XkUgKtiGaDrj5iN42yVQbs1zKO45iV6/Lj2gGn1lgQPqvUSg+3v4gtBply2ZPJAnBqE/5D+555qer52YhVepzHqdQxBK5BG2/2Ja+j0gZkPYgzpG5TUyXTs+TjcGH7mk0mQlaPmEPJm/UI4cQD0chXNkPezLn1Fi+0n3lb6bXZkY+5jQ7oqydGz7jniR1HMxZROXL/ZT6I2jTHtGSAGERjCSmMaAhGYp0iKQh4HoMO7OijkE0Z7X6WQafTjgA1ftPP/RPR1z7crkAqbFze65102OkI4XiX4sLnyZM2shjexoG9Tazdjf9TyTWtvdz/wCU3HUOoDLjyBiFPYfMw2oA3sRyLlyhSdlsJJ8roGbiO0h3Z8Y+ajcrS69P9LGoZchNFTY+ZiytWXxTZtul4yMSgeV/6nX6Z9RtzsQIdo9KVx8C9oofMA1ebJZUKwHbtMluzbFJR5C06ZiUC2UmObAqEBa/iZ/OcystByCeZf8AS8TuF3Ag/MScZJ9ki19FzoMRZfMZrtPZG7t5lx07DsXmd12n3qahUfsnPozbZMeLk7OO9yTD1fETtXYa4PxA9V0ZshcWwvtBen+mCrNudvu+e0aKVdgbf0ajBkxZqKlLHNCWWlN8VwO3zKXp/QPoHfvJ81cvsKbQB7cSJ8hZUeoVtQIB0LBtcluB3Etuspu2yPHiUINp+6uajXSsXbfBZqwIpaPvG5IHoCQzA2YY8vg7VmacaltIyI0iPnDHFIyIwiSkRpEACIxTpE5CQ8y9P6n6FKDQ/qXHU9Yr4y+4Egdr7yp0fTy2F8nIrkSi1+VkZks12nV0O3Y05cmHNp8bybtlsj1mU5i5srX+5VkEX5krub/feRvkoVEzbG3wX4048IiyGgZsvQmn3gn5mLPKkz0H/wCOgDjc+RORqOUzZg4kehafGFVRQPvHvpFbnaP6kWnapYIwYTPXBqS4ABoUvlVP8SQadVI2gCEPkAuQ43+7mRhoOwrQ9pKQCDchXIOKI+Y/K4Ckg+IwrQPlxjntIBi54kTar7iD7wnHnBHiKNRJiQwgLUixuDJS/EehWCa9dy1K3ToVY3ZB7SxymzEcf4mK+XRE2lYtMm0k13kzG4+wFEi38maoqlRjk7dnanCJwPZo8TpPtGAMZqjPy78R5EaTAQYRRnYjFIAwA1f09PkFeJitVl+tmrsC1TSa/NtRkHniZvZT383Lce6PMJAlJJ2xmqwjHVc3An5hOvvcpgjPcuySXO5ciJe0xhPBE2HonVfTBT3Mx5WzLzomo+iRMM1cWXwdSR6vgzcA+/MOxaihUynTtd9UL/Uu9OeLMyM3XyWDWQX9uZQdR6xkxsEVTXa6l9iygCjUH1GnxubbZcKIV+l6k7CyDfeWel1L5OCGA7SFdPjQiitS002XGoHKCFEsHzaXjd/MEDFTXMumzoVI3L8cyuKBmNUeZGgWSafMeLhm+xBhgqoQvAEKFIsrmxxJlawJIMYaO2gcS6EPZRLJxSG3EBOnmNMtoou2MyJfIkIcqak4idQYQkYNxpMayEGIN4gIIzsVRSAPI+s6j6V3zJOkdP8A8zBk1PYJzK31M+5wg88TU+lqxdJ1INbihIlsJbZUhZRtcmI6mArlRzRqAPzJtRk3ZMhP/M1/cHc2eJZlfxBFbRycMBLjSYwFLfzKNTbLLvESMZ/UyMtiaL09qPuq/M22Nwygj2nlvSNXsyBSe7f9z0DR6m1SubHMz5TVjY/Val1NAGCnK7nuyy5UKy2QDAs2PvtErtF0SLDiZq+4/wBw/HoWI4Y/3K5HZW7NLnQOzVYIhTQ9g+Tp2Redze/eE6BSpprNSy7ipGMQWz2uRoqb55JS10I5TVXBUWiSZIMm4gDxHX7CSaSssB2BnLnF/ER6zUY+GQu9SP6p9oRkAMiCiFcAv2jgMRMREVQBGGRMJORGMJCDB2nZ2opCHgvVcu7MjSyw9XOPA+EdmFGUuVt7AzuQbamjBFU2yub54IXNlj7m4xeLj3MjJoRMz9DR6O4xbr+5e7guP+JT6NNzAyw1J4AHtM77LI9AeN6yBx4Nzb9E124KCe3Ew6LtuXnQ3Pg/qJkjwWY3yekabKCByJY4sasOamKw684SA189pYp1iwOambk0pmpXToD2UwhEWuABMunVwO5H9yZetqB3H9xlYGzRmlkGbOPf/czep9QV2+6C4upNnaqYCR8A7/U0baqzQ58QzRAd24vmU2kIUbmIJ7wtNQcjALwF/wByzGm5FWbJCGP5svx2nTA0zUoB7+ZKmUHzNssbic3Hq8c8m1EjCM2xxYe8ZKzSq6OmNJiJjZAiJjLnTG1IQ7cU5FIQ+ekPIj80Yoqddppx0ocFTISbiInGPtEp8TPNtssSYboeOPeEalKo3BdMKhqYWzMEFm+L71K2vXsdf2wXGpchACb4mm6J00oVLGr55k/S+hDEv1GonuPeO1mo2Alft2fxL1g+LlMwS1v/AKLHjfHtlh1PSCkKkcDmU7oRwDAcvqa7Q2a4gJ6xZJ9+05/imdlZMZbnG3fcf7nVxsx/Oq+ZSP1Y/MgbqTHhd1x9khPJFdGuxqqC3YNXzc6euLiIxqu4nixMtosOXUN+TgTV9L6OFpslOe4vmXYtK5u2zFqfyMMHa5+i16ez56eyoPNdpotNSL8+fmU+IhKCigPaG4mOSvFd508eGMFSR5jUazLnnc3/AIHhy54hCAgd4Gr7B7+8eubd8R3FehYScXa7Cy5Hm44ajxBFfb3NyVcin2EXxxfaL1qMl3u5DUexOniDrkAk4axf8zLkxpdI6ul1in8cj5OCInmQrn3MU9uJKg2m+8p2t+joOS9nLinW5NzkhD57QWLjH5kgcAVIyPIl6aURf6GXt7zvLG1BMK0XT31DAAEXxNh0r0rtKO9Ed+ZVHHKfRVlz4sXM2Zvp3TXzigGXx2mt6R0j/HUh/uY8gnxNJg0mPAoCqoNeBAupZgiuRxQuaoadY/m+zk6j8hLMvFCNIAykqwQGweJUeoE2rxxYsyx6Zn+szFuaPEg9RaQuhZb4EeT3Y90UU47x6hQm+jAOKZj8zgG74j2RlZgQ3BqT6fStmYKAVvjtU59X0ejckuX0DohY7QL5qXnS+klqZh88yy6b0ZcQDOQ18/qXGxEAogV3mjHi28yOZqNb/HEuPsZotKqAUACJZY0L9jQEB/zUHAr5iXWg/iamm0uzkSU5Pc4luCEq/u/3Jk1IHbi5RjV7b3fd7eYxuo1dA/EbdFegeKT/AFRofrheWINyJ+oqt0QJnm1Tv7j2kL6XJkqiRcVyfpFiwpL5TLjUdfCnaBu8ccwjp+qbOb5Ud4B0/ohsO/3eeeZdhUwKAoAPaRbn2SSxJfFW/sPxgkAX+4UrbRyblVjzebr2iXUFmIvgQ19oSLfaLHftN+8lTU7eTzAEyb/thKYe18yOK+geWUumT/5o9oo5dOK8TsXbD2Wb83/Q8t6j6DyabG+VmsILMzuh0BfKFPKhqM9l9ZavbpsuLyVIE8y6NjNu577rExYbnw+j0OpyrFBtGi6ZpceBV+0X3uWoz8fbxKzSvxzz7QtDZE6cEoqjzGeUpTuTClyE95W9VB2t+pYHipV9Uy7XRDyDwYuRxUQ6ZJ5UkuSs6XqfpMykHky/3rkQ7q5HmVz7EAYKL79oJm1pYFEtfAlcXt4bNGaCzS3xVP7INdpMaMXoHmzAX6hjThUAI4sDzJH0rubLEgxN00AWR+5U02/jE2QlBRUZysGfXuwtSwHiQrkyZTtBbvUITT01CWuk0oUXXMSEJTdykX5c0MUdijyVmLp2Q0Sx5h+Hp7jyZZY0k4FS9YkvZzp6rI1QJi0pFbuYUmnWuQJIouNyNUfoz75S9jHRV7AQjSrRs/xIMSFzzCHfaABCBrbx7Dv8oKKEDfMcjceOYMxJkmIbQSfMAdtf6S5MhYKimiO8KxvSgefMrsR+4kw5DCLMOxOE5MIx6qz3/UqcmXwPEYmejX9yAiq5NFh1XMUrMOTi4oKQ1sP9V6T6yOwrtMBptL9NmHzN76jD41YWSK5mGR7Y/uZ9NFKB1NdknLI1XBZaRNxCjueJaDRMoBIPuOIL0dQc2IHsTzPSv8XG6J9o/H2lmbP4Wk0Z9Lo1qVJuXKPOc24G6PHxKbX5N+RPtPB9p60/TMZBBVefiV2f05jY2AgPcSqerjJU4GnF+MyY25qaMGuEMoBHiAanQ7TYm91Hphu6kACVOr6Q2K9wJqXrJhnzuMUtPqcL/W4mUwadh3uSZ1pSp7kUJdJjCmiKlV1Wg6Be18xpRUFdiYZ+bLTAdDoSzEn3sXLMabZQkmJgqrXBqOLX3kUKEy5pTlbOJjqSFIgwjXb2j0im2Mc1GDGWIkq4y0mChBBQydehjUi/NQEuWaP1OXmosC7oo8U0tz7JQlgf7j3PAElxLQkGbvCDduYk45kwfiQr2nGybYCNWdbJyRHYV5syBV3Hd/MnGShXtIR/QYX2gTkrdVrNoqKDcg+Of0ekda0wyabKxFsF4nlTYiuZvbd/3FFMOlfJ3vyKShZedLYDNi/c9L0xtEPxFFLNZ2jN+J/kSv8Agx8jtMd1Tq74MoG41cUUq02OM5/I2a3JKGNyi+Sx6d6hD7Ubm+DLs48eoUEgc8xRSZ8cYS+JNBmnni1kKLrXRxtd0AWhc826gCmXa3NNQiilmGTcabK9Vp8cLyRVMJxHcok6GKKbv4UcGaqNoeFMnw4778xRQpFJNkIUcSt1OU81FFBIuj2BKdzcw/EKAqKKKhsoUO0Hyd4ooWJEYTxICbMUUBZ6JUNRmfJSmu8UUVkh+yKvTAvlbdyPEUUUTHyi7PNqfB//2Q=="
                    />
                  </ListItemAvatar>
                  <ListItemText
                    primary="Dock Of Shame"
                    secondary={
                      <Typography component="span" variant="body2" color="textSecondary">
                        @FavDockOfShame
                      </Typography>
                    }
                  />
                  <Button color="primary">
                    <PersonAddIcon />
                  </Button>
                </ListItem>
                <Divider component="li" />
              </List>
            </Paper>
          </div>
        </Grid>
      </Grid>
    </Container>
  );
};
