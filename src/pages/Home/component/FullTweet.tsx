import React from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import { Tweet } from '../../../components/Tweet';
import { fetchTweetData, setTweetData } from '../../../store/ducks/tweet/actionCreators';
import { selectTweetData } from './../../../store/ducks/tweet/selectors';
import { useHomeStyles } from '../theme';
import {  selectIsTweetLoading } from '../../../store/ducks/tweet/selectors';
import { CircularProgress } from '@material-ui/core';

export const FullTweet: React.FC = (): React.ReactElement | null =>  {
    const dispatch = useDispatch()
    const tweetData = useSelector(selectTweetData)
    const isLoading = useSelector(selectIsTweetLoading)
    const classes = useHomeStyles()
    const params: {id?: string} = useParams()
    const id = params.id;

    React.useEffect(()=> {
        if(id){
            dispatch(fetchTweetData(id))
        }  
        return () => {
            dispatch(setTweetData(undefined))
        }    
    },[dispatch,id])

    if(tweetData){
        return <Tweet classes={classes} {...tweetData}/>
    }

    if(isLoading){
        return  (
            <div className={classes.tweetsCentred}>
                <CircularProgress />
            </div>   
       ) 
    }

    return null
}