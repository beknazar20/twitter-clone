import React from 'react';
import classNames from 'classnames';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import IconButton from '@material-ui/core/IconButton';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import ImageOutlinedIcon from '@material-ui/icons/ImageOutlined';
import EmojiIcon from '@material-ui/icons/SentimentSatisfiedOutlined';
import { useHomeStyles } from '../pages/Home/theme';
import { useDispatch } from 'react-redux';
import { fetchAddTweet } from '../store/ducks/tweets/actionCreators';

interface AddTweetFormProps {
  classes: ReturnType<typeof useHomeStyles>;
  maxRows?: number;
}

const MAX_LENGTH = 280;

export const AddTweetForm: React.FC<AddTweetFormProps> = ({
  classes,
  maxRows,
}: AddTweetFormProps): React.ReactElement => {
  const dispatch = useDispatch()
  const [text, setText] = React.useState<string>('');
  const textLimitPercent = Math.round((text.length / 280) * 100);
  const textCount = MAX_LENGTH - text.length;

  const handleChangeTextare = (e: React.FormEvent<HTMLTextAreaElement>): void => {
    if (e.currentTarget) {
      setText(e.currentTarget.value);
    }
  }; 

  const handleClickAddTweet = (): void => {
    dispatch(fetchAddTweet(text))
    setText('');
  };

  return(
    <div>
      <div className={classes.addFormBody}>
        <Avatar
          className={classes.tweetAvatar}
          alt={`Аватарка пользователя UserAvatar`}
          src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxATEhAQEBQVEBAVFhobEBUVGRsQHBAgIB0iIiAdHx8kKDQsJCYxJx8fLUYtMSwuMDA4Iys/QD8uTTQ5MC4BCgoKDg0OFw8QFTcdFhkrKystNzcrLjM3Mis3My83LTc1OC03MjY3Nys0MzM3NzMzMSsrKy0rKysrKyszKysrK//AABEIAGQAZAMBIgACEQEDEQH/xAAbAAABBQEBAAAAAAAAAAAAAAAEAAIDBQYBB//EADQQAAEDAgQDBgQGAwEAAAAAAAEAAhEDIQQSMUEFUWEGInGBkaETMrHBFELR4fDxI3OSYv/EABkBAAMBAQEAAAAAAAAAAAAAAAIDBAEABf/EACYRAAICAgIBAwQDAAAAAAAAAAABAhEDMRIhBBNBUTJxgZEUIjP/2gAMAwEAAhEDEQA/ANNUxRT6FYwN51MoZlAnSUY2jAH5RvzKpnKCVHlpEnxAFw4gISu+NFAa8a6IFC+zew5+IOyj/FIduIBi0Ap/ddobouKW0cFU8VJi6fUqnn6qvjKYOqQru8l1U7id9y0w1YTJPgp/iE9AqdryYgQd1MMTlgFdKTb7O6LB74Q1WoofxIdaYKflsji0Y7Iw89UkwjqknWgaJw6NRbnomOqhxvqgzVIzF0ET3Y3EWQ9N8QSZc4gD9lFwsbZY1GIE0iHEkyNp2UtPHjNEzGsQUaWNdpbohWSUNnVZVhhGt1LQpSZ9CnVsOZ/87zumzUyvFJhqPaJyj2CY8tro6MW3SJ+9pYjZCPJB0PkNFlH9ouINqkPpNDQ6HAg291pcDj6denmZBGjte4eSyMhksTjsOFcWtE7xChqPBEm3Qx90NVht9hY6n+D9EXVIAFrbHVH1sXRBUqGdj7KOnjy1wGax5nRNfXudEBiw2QRBB1E/RNxxTdMxovfxvMXSVHRxpAAOV3IlJM9FghtTEPfDAMsAT0HNSFkNGWS4juk/kHPxSrvLJa5xjUg6lRhhIYXSdwSRN4vA2UnL9DKDcO1pbm+XLtuF12Ii4IOsN0J80LUe0WgZTqLNn1QlauTmcIkeeS/3QVyZui6GKMTrbQqF1drGYipTzCoWiWgxNwPLVVdGqTfSZkkfvySOKDrN+VwO8aDks9Orodha5oqcRUqEDuwXXcPmj3CJ4Th4LmtEXzctlCS4OBMZRvKOoYYvaH0ntLpMsNpvz/VZCfdWVeQkodhZqOBvI6j+aLmJqOH+Q95rRYCZPiENVq1QQKjYBtdsHzOiLy2AgiZuDbomt1R5wLSr5xIBHjuh695BEQpsZRc1wIFjaWyQPFMc3mbhOjJbRlFcYEgkgpIp+HkzP3SVHrIHiaCjXDrOkRrp9Rv5qOu0tJLW5tIdOtt+W6FrPaxsiTMZWAi3Ow1RLMSSyYA0jM7LPlC8nvfsOBqtYZS5wIMxBuecgSoThjuCAIJgE7zHXX2U4LQWuIl2uUQY6jkkakB9aZaJeQ4QIAk25mEcZVo2MeTohx/F8JSIZWdLz8zWiMvRxG6Ax3H8IKNRlJobnaRmF3CepWAfWLnZ3d5xJLp3MrtSoSZOvpCake5i9PGqUF9yRuOqtsHGPVaHsx2jp0gWV2kjMS17dRPPnussUl3FC2k9qz12hxlrg0sh7DMy4WHMhE1qgc0GkA+NWg62/peT8Ar5cRSN4Lod4Gx+q9Ya1tKmWhhkTZskx0ESpM3GDRDnwxUrj0gXBUc8EtLS6c4J+ykdgabYDSXSYH5Y6A+yJo12gRABEHvd0xFyfJR0uIgtLqdMvyzkgTveCkvNJ+/QtQigevRptMOAYeRhv1KS7Xxtdzjla9oFvl/YpLPVfybUBuM4EXuDxUJc0WB35Sg+JcLrODGtcS43dLha2gCOwVUtgNfmNgXEzmO4hGuqOJc3OG1MsxE5b66rl5E4vdpCvTKangcRJLg1rbACcxIA013Qva406WGqFoio8Bg5Xub72B9VbiqLl7LCWy8lpeZ1A5FYjtvxEP8AgMa004aXOYZlpNh7AeqbiySnIo8fGuV/BlaWo809yjw4uU95ViL1o4ugJBcdoVzNO4V5Dg7cXXsJcfiUqjZaCJeA3NtoTNl43TkTNjoV7Hg3k0qZBnuNPjZSeSk6Jc2kFANkuytzc4EobHYh2WKRGZpBLRqRy/tOr0szXAXzAj2hBcNwvw6Z+J81s7rA9JI5KVRT2IH0ONANArf437tJkjlKSe7D0SXOIBJMkyb28VxdS+DB2Hwz2OAc4GmL/KBJOnhH6J9bH5Q6o6GwQDfUTr7qLimMNOmCYEwHT3ot4LEcTxVQFxNQPmRBJm4+1vQpHB5ZVpG3RvKxo125CZE2g6kFeY9ra4diasfK05W3mzbDc8le9lsW/M2/cDu9Fy2BJPOICz1LDOxOJcGDMSXvIiJAk+UxHmrvExvHytlOJVByAaQgdUk50qN3VXlHsdlOpszOawmJIBPK6jzJzATAA106oWC2TY+kW1qzCZy1HCecEr0XglaszDZnhuUMaWbWifOy8+4qyMRXEkxUdc6m+69i4Zh2fBoAtkfDbz5bqTyJxUVfuTZH/VFJwqvnaarS5rXF0tddpJ3HRG16ZeA1wD2Gz5/RWjsG3ujKA1sZQAIEbqctiYgDnAsonnTfSJ+Ri8b2WxD3ufTqZGnRpJZl6JLX1mGT7pLP5Mvgy0UHaymBh3xqSLnxWW4ZwtmIpOdULpaWBsEADM/KduQSSTfF0wQvAcLZRw+Jr03PFRrSGmQYmAduqznDHFtDHVWkioGsYD0e+HeoC4krV9P5RYv8SjFQqVrkkk9h49DgiODGcRh2m4NVgP8A0EkkEtMKWg/tOB+Nxf8Atd9V7Lg6TRTpQB8rfokkoPJ+iBPk0iSooGHS2kx7pJLzpbEMKpNsupJLQT//2Q=="
        />
        <TextareaAutosize
          onChange={handleChangeTextare}
          className={classes.addFormTextarea}
          placeholder="Что происходит?"
          value={text}
          rowsMax={maxRows}
        />
      </div>
      <div className={classes.addFormBottom}>
        <div className={classNames(classes.tweetFooter, classes.addFormBottomActions)}>
          <IconButton color="primary">
            <ImageOutlinedIcon style={{ fontSize: 26 }} />
          </IconButton>
          <IconButton color="primary">
            <EmojiIcon style={{ fontSize: 26 }} />
          </IconButton>
        </div>
        <div className={classes.addFormBottomRight}>
          {text && (
            <>
              <span>{textCount}</span>
              <div className={classes.addFormCircleProgress}>
                <CircularProgress
                  variant="determinate"
                  size={20}
                  thickness={5}
                  value={text.length >= MAX_LENGTH ? 100 : textLimitPercent}
                  style={text.length >= MAX_LENGTH ? { color: 'red' } : undefined}
                />
                <CircularProgress
                  style={{ color: 'rgba(0, 0, 0, 0.1)' }}
                  variant="determinate"
                  size={20}
                  thickness={5}
                  value={100}
                />
              </div>
            </>
          )}
          <Button
            onClick={handleClickAddTweet}
            disabled={text.length >= MAX_LENGTH}
            color="primary"
            variant="contained">
            Твитнуть
          </Button>
        </div>
      </div>
    </div>
  );
};
