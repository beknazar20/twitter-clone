import React from 'react';
import { Divider, List, ListItem, ListItemText, Paper, Typography } from '@material-ui/core';
import { useHomeStyles } from '../pages/Home/theme';
import { selectTagsItems, selectIsTagsLoaded } from '../store/ducks/tags/selectors';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
interface TagProps {
    classes: ReturnType<typeof useHomeStyles>;
}
export const Tags: React.FC<TagProps> = ({ classes }: TagProps): React.ReactElement | null => { 
    const items  = useSelector(selectTagsItems)
    const isLoaded  = useSelector(selectIsTagsLoaded)

    if(!isLoaded){
       return null  
    }
  return  (
    <Paper className={classes.rightSideBlock}>
    <Paper className={classes.rightSideBlockHeader} variant="outlined">
      <b>Актуальные темы</b>
    </Paper>
    <List>
      {
          items.map(obj => (
            <React.Fragment key={obj.name}>
                <ListItem  className={classes.rightSideBlockItem}>
                <Link to={`/home/search?q=${obj.name}`}>
                    <ListItemText
                    primary={obj.name}
                    secondary={
                        <Typography component="span" variant="body2" color="textSecondary">
                        Твитов: {obj.count}
                        </Typography> 
                    }
                    />
                     </Link>
                </ListItem>
               
                <Divider component="li" />
              </React.Fragment>
          ))
      }
 
    </List>
  </Paper>
  );
};
